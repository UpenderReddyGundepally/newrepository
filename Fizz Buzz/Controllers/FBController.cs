﻿using Fizz_Buzz.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Fizz_Buzz.Controllers
{
    public class FBController : Controller
    {
       

        public ActionResult Index()
        {
            Number model = new Number();
            return View(model);
        }

        [HttpPost]
        public ActionResult Submit(Number num)
        {

            List<ResultM> lResult =new List<ResultM>();

            if (ModelState.IsValid)
            {

                int inputNumber = num.inputNumber;
                string output = string.Empty;

                for (int i = 1; i <= inputNumber; i++)
                {
                    ResultM r = new ResultM();
                   
                    if (i % 3 == 0)
                    {                      
                        r.name = "fizz";
                        r.labelColour = "blue";
                        lResult.Add(r);

                    }
                    if (i % 5 == 0)
                    {
                        r.labelColour = "green";
                        r.name = "buzz";
                        lResult.Add(r);
                    }
                

                }            

                return View("Result", lResult);
            }
            else
            {
                return View("Index");
            }

           
        }



    }
}
